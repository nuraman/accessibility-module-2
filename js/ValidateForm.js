function ValidateForm() {
  this.errorList = [];
  this.userList = null;

  this.validate = function (form, userList) {
    this.userList = userList;
    const inputList = form.querySelectorAll('input[type=text]');

    for (i = 0; i < inputList.length; i++) {
      this.checkInput(inputList[i]);
    }

    if (this.isErrorsInForm()) {
      return {
        errors: this.errorList,
        valid: false,
      }
    }

    const nickName = form.querySelector('#input-nickname');
    if (!isNicknameValid(nickName, userList)) {
      this.errorList.push({
        element: nickName,
        message: 'there is a user with the same nickname, please enter another',
      });

      return {
        errors: this.errorList,
        valid: false,
      }
    }

    return {
      errors: [],
      valid: true,
    }
  }

  this.checkUserNameAndSurname = function () {
    var form = document.querySelector('#contact-us');

    var name = form.querySelector('#input-name');
    var username = form.querySelector('#input-username');
    if (isNameAndUsernameExist(name, username, this.userList)) {
      var birthdayContainer = form.querySelector('#user-birthday');
      var birthDayText = form.querySelector('#birthday-text');
      var input = form.querySelector('#input-birthday');

      birthdayContainer.style.display="block";
      // birthDayText.textContent = "Please, enter you birthday";
      // input.focus();
    }
  }

  this.isErrorsInForm = function () {
    if (!this.errorList.length) {
      return false;
    }

    return true;
  }

  this.checkInput = function (input) {
    switch (input.getAttribute('id')) {
      case 'input-nickname': {
        return this.checkName(input);
      }
      case 'input-username': {
        return this.checkName(input);
      }
      case 'input-name': {
        return this.checkName(input);
      }
      case 'input-phone': {
        return this.checkPhone(input);
      }
      case 'input-address': {
        return this.checkName(input);
      }
      case 'input-birthday': {
        return this.checkBirthday(input);
      }
    }
  }

  this.checkName = function(input) {
    if (!isInputValid(input)) {
      this.errorList.push({
        element: input,
        message: 'please enter correct value'
      });
    }
  }

  this.checkPhone = function(input) {
    if (!isPhoneValid(input)) {
      this.errorList.push({
        element: input,
        message: 'please enter correct phone number, number should be only digit'
      });
    }
  }
  
  this.checkBirthday = function (input) {
    if (!isBirthdayValid(input) && !input.classList.contains('hide')) {
      this.errorList.push({
        element: input,
        message: 'please enter correct birthday, birthday should contain only four digit'
      });
    }
  }
}