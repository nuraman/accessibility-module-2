(function () {
  var btnList = document.querySelectorAll('.modal-button');

  btnList.forEach(function (el) {
    el.addEventListener('click', onHandleClickModal);
  });

  function onHandleClickModal(e) {
    var modalName = this.getAttribute('data-target');

    if (modalName) {
      onFocusModal(modalName);
    }
  }

  function onFocusModal(id) {
    var modal = document.querySelector('#' + id);
    modal.setAttribute('aria-modal', 'true');

    modal.focus();
  }

  var closeBtnList = document.querySelectorAll('.modal-close');

  closeBtnList.forEach(function (el) {
    el.addEventListener('click', onHandleCloseModal);
  })

  
  function onHandleCloseModal(e) {
    this.closest('.modal').setAttribute('aria-modal', 'false');
  }
})()