function notifySuccessForm () {
  var liveRegion = document.querySelector('#success-message');
  liveRegion.classList.toggle('visuallyhidden');
  liveRegion.textContent = 'USER SUCCES SAVED';

  setTimeout(function () {
    liveRegion.textContent='';
    liveRegion.classList.toggle('visuallyhidden');
  }, 3000);
}