function UserList() {
  this.userList = {};

  this.setUser = function (form) {
    const nickname = form.querySelector('#input-nickname');
    const username = form.querySelector('#input-username');
    const name = form.querySelector('#input-name');
    const phone = form.querySelector('#input-phone');

    this.userList = {
      ...this.userList,
      [nickname.value]: {nickname: nickname.value, name: name.value, username: username.value, phone: phone.value}
    };
  }

  this.getUserList = function () {
    return this.userList;
  }
}

