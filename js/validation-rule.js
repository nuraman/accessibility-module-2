function isNicknameValid(nickname, userList) {
  if (Object.keys(userList).includes(nickname.value)) {
    return false;
  }

  return true;
}

function isNameAndUsernameExist(name, username, userList) {
  let exist = false;

  const keys = Object.keys(userList);

  for(let i = 0; i < keys.length; i++) {
    if (userList[keys[i]].name === name.value && userList[keys[i]].username === username.value) {
      exist = true;

      break;
    }
  }

  return exist;
}


function isInputValid(input) {
  if (!input.value) {
    return false;
  }

  return true;
}

function isPhoneValid(input) {
  var redex = /\d+/;
  var value = redex.test(input.value);

  return value;
}

function isBirthdayValid(input) {
  var redex = /\d{4}/;
  var value = redex.test(input.value);

  return value;
}

function isEmailValid(input) {
  const regex = /[\s0-9a-zA-Z]+\.[a-z]+/g;

  return regex.test(input.value);
}