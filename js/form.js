(function () {
  var contactForm = document.querySelector('#contact-us');

  contactForm.addEventListener('click', onHandleClickForm);

  contactForm.addEventListener('submit', onHandleSubmitForm);
})()

var userList = new UserList();

function onHandleClickForm(e) {
  switch (e.target.id) {
    case 'contact-agree':
      return changeStateForCheckbox(e.target);
  }
}

/**
 * validate form and save
 *
 * @param formElement
 */
function onHandleSubmitForm(e) {
  var form = e.target;

  var validateForm = new ValidateForm();
  var { valid, errors } = validateForm.validate(form, userList.getUserList());

  e.preventDefault();

  if (errors.length > 0) {
    showError(errors);
    return;
  }

  if (isUserNameAndSurnameExist()) {
    showInputBirthday();

    return false;
  }

  // user is valid and we can save
  return successUser(form);
}

function isUserNameAndSurnameExist() {
  var form = document.querySelector('#contact-us');
  var name = form.querySelector('#input-name');
  var username = form.querySelector('#input-username');

  var birthday = form.querySelector('#input-birthday');

  return isNameAndUsernameExist(name, username, userList.getUserList()) && !birthday.value;
}

function showInputBirthday() {
  var form = document.querySelector('#contact-us');
  var birthdayContainer = form.querySelector('#user-birthday');
  var birthDayText = form.querySelector('#birthday-text');
  var input = form.querySelector('#input-birthday');

  birthdayContainer.style.display="block";
  input.classList.toggle('hide');
  birthdayContainer.focus();
}

function successUser(form) {
  var error = document.querySelector('#error-form');
  var input = form.querySelector('#input-birthday');
  error.classList.add('visuallyhidden');

  userList.setUser(form);

  if (!input.classList.contains('hide')) {
    input.classList.add('hide');
  }

  clearForm(form);

  notifySuccessForm();
}

function showError(errorItemList) {
  const error = document.querySelector('#error-form');
  // remove hidden class
  if (error.classList.contains('visuallyhidden')) {
    error.classList.remove('visuallyhidden');
  }

  const ol = error.querySelector('ol');
  // clear previous errors
  clearErrorList(ol);
  // show errors
  errorItemList.forEach((item, index) => {
    createErrorItemList(ol, item, index);
  });

  // focus to error area
  error.focus();
}

function clearErrorList(myNode) {
  while (myNode.firstChild) {
    myNode.removeChild(myNode.firstChild);
  }
}

function createErrorItemList(parent, item, index) {
  const { element, message } = item;

  const name = element.getAttribute('name');
  const id = element.getAttribute('id');

  const li = document.createElement('li');
  const a = document.createElement('a');
  a.setAttribute('href', '#' + id);
  a.setAttribute('aria-labeledBy', index + 'error');
  const p = document.createElement('p');
  p.textContent = name + '' + message;
  p.setAttribute('id',index + 'error');
  a.appendChild(p);
  li.appendChild(a);
  parent.appendChild(li);
}

function changeStateForCheckbox(el) {
  if (!el) return console.log('nothing change');

  var isChecked = el.getAttribute('aria-checked');

  if (isChecked === 'true') {
    el.setAttribute('aria-checked', false);
  }

  if (isChecked === 'false') {
    el.setAttribute('aria-checked', true);
  }
}

function clearForm(form) {
  const inputList = form.querySelectorAll("input[type=text]");

  inputList.forEach(function (input) {
    input.value = '';
  })
}