(function () {
  var email = document.querySelector('#input-email');
  var emailValidate = new EmailValidate();
  email.addEventListener('focus', function () {
    emailValidate.reset(event);
  });
  email.addEventListener('blur', function () {
    emailValidate.startValidation(event);
  });
})()

function EmailValidate() {
  this.validationEnd = false;
  this.validationResult = false;

  this.reset = function (event) {
    this.validationEnd = false;
    this.validationResult = false;
  }

  this.startValidation = function (e) {
    var form = document.querySelector('#contact-us');
    var emailError = document.querySelector('#email-error');

    if (!this.validationEnd) {
      var email = e.target;
      email.classList.toggle('visuallyhidden');
      const countContainer = document.querySelector('.count');
      const p = document.querySelector('.count-value');
      p.classList.toggle('visuallyhidden');
      p.textContent = 'Start validation email';

      setTimeout(function () {
        p.textContent = 'validation end in 2 second';
        focusOn(p);
      }, 500);

      setTimeout(function () {
        p.textContent = 'validation end in 1 second';
        this.validationResult = isEmailValid(email);

        focusOn(p);
      }.bind(this), 1000);

      setTimeout(function () {
        if (this.validationResult) {
          p.textContent = 'validation end success';
        } else {
          p.textContent = 'error in email validation';
          emailError.textContent = 'error in email validation';
        }
        this.validationEnd = true;
        p.classList.toggle('visuallyhidden');
        if (email.classList.contains('visuallyhidden')) {
          email.classList.remove('visuallyhidden');
        }
      }.bind(this), 1500);
    }
  };
}

function focusOn(email) {
  email.setAttribute('tabindex', -1);
  email.focus();
  email.removeAttribute('tabindex');

  const waitMessage = document.querySelector('#email-message');
  waitMessage.textContent = "please wait";
}