(function() {
  var burger = document.querySelector(".burger");
  var menu = document.querySelector("#" + burger.dataset.target);
  burger.addEventListener("click", function() {
    burger.classList.toggle("is-active");
    toggleAttribute(burger, 'aria-pressed');
    menu.classList.toggle("is-active");
    toggleAttribute(burger, 'aria-expanded');
  });

  burger.addEventListener('keydown', keyPressListener);
})();

document.querySelectorAll("#nav li").forEach(function(navEl) {
  navEl.onclick = function() {
    toggleTab(this.id, this.getAttribute('aria-controls'));
  };
});
document.querySelectorAll("#navbarMenu li").forEach(addEventListenerToMenuItem);

addEventListenerToTabs();

function toggleTab(selectedNav, targetId) {
  var navEls = document.querySelectorAll("#nav li");
  navEls.forEach(function(navEl) {
    if (navEl.id == selectedNav) {
      navEl.classList.add("is-active");
    } else {
      if (navEl.classList.contains("is-active")) {
        navEl.classList.remove("is-active");
      }
    }
  });

  var tabs = document.querySelectorAll(".tab-pane");

  tabs.forEach(function(tab) {
    if (tab.id == targetId) {
      tab.style.display = "block";
    } else {
      tab.style.display = "none";
    }
  });
}

var keys = {
  end: 35,
  home: 36,
  left: 37,
  up: 38,
  right: 39,
  enter: 13,
  esk: 27,
  tab: 9,
};

function addEventListenerToTabs() {
  const tabList = document.querySelectorAll('#nav li');

  tabList.forEach(addEventListenerToTab);
}

function addEventListenerToTab(tab) {
  tab.addEventListener('keydown', keyDownListener);
  tab.addEventListener('keyup', keyUpListener);
}

function keyDownListener(event) {
  deactivatetab(event);
}

function keyPressListener(event) {
  const key = event.keyCode;

  switch (key) {
    case keys.tab: {
      handleTabPress(event);

      break;
    }
    case keys.enter: {
      handleEnterPress(event);

      break;
    }
  }
}

function keyUpListener(event) {
  const key = event.keyCode;
  const tabList = document.querySelectorAll('#nav li');

  switch (key) {
    case keys.left: {
      activateTabOrientation(event, 'left');
      changeTabindex(event, -1);
      break;
    }
    case keys.right: {
      activateTabOrientation(event, 'right');
      changeTabindex(event, -1);
      break;
    }
    case keys.home: {
      activateTab(tabList[0]);
      changeTabindex(event, -1);
      break;
    }
    case keys.end: {
      activateTab(tabList[tabList.length - 1]);
      changeTabindex(event, -1);
      break;
    }
  }
}

function changeTabindex(event, value) {
  event.target.setAttribute('tabindex', value);
}

function deactivatetab(event) {
  const link = event.target.querySelector('a');

  if (link) {
    link.setAttribute('tabindex', '-1');
  }
}

function activateTabOrientation(event, orientation) {
  const tabList = document.querySelectorAll('#nav li');

  if (orientation === 'left') {
    const prevElement = event.currentTarget.previousElementSibling;

    if (prevElement && prevElement.getAttribute('role') === 'tab') {
      activateTab(prevElement);
    } else {
      activateTab(tabList[tabList.length - 1]);
    }
  }

  if (orientation === 'right') {
    const nextElement = event.currentTarget.nextElementSibling;

    if (nextElement && nextElement.getAttribute('role') === 'tab') {
      activateTab(nextElement);
    } else {
      activateTab(tabList[0]);
    }
  }
}

function activateTab(element) {
  const link = element.querySelector('a');
  link.setAttribute('tabindex', '0');
  toggleTab(element.getAttribute('id'), element.getAttribute('aria-controls'));
  link.focus();
}

function addEventListenerToMenuItem(item) {
  item.addEventListener('keydown', keyPressListener);
}

function handleTabPress(event) {
  const element = event.currentTarget;
  const burger = document.querySelector(".burger");
  const menu = document.querySelector("#" + burger.dataset.target);

  if (element.getAttribute('class') === 'navbar-item' && burger.classList.contains('is-active')) {
    const nextElement = element.nextElementSibling;

    if (!nextElement) {
      burger.classList.toggle("is-active");
      toggleAttribute(burger, 'aria-pressed');
      menu.classList.toggle("is-active");
    }
  }
}

function handleEnterPress(event) {
  const element = event.currentTarget;
  const burger = document.querySelector(".burger");
  const menu = document.querySelector("#" + burger.dataset.target);

  if (element.getAttribute('class') === 'navbar-item') {
    burger.classList.toggle("is-active");
    toggleAttribute(burger, 'aria-pressed');
    menu.classList.toggle("is-active");

    const nextElement = element.nextElementSibling;

    if (nextElement.getAttribute('class') != 'navbar-item'){
      burger.classList.toggle("is-active");
      toggleAttribute(burger, 'aria-pressed');
      menu.classList.toggle("is-active");

      //expand menu
      toggleAttribute(menu, 'aria-expanded');
      toggleAttribute(burger, 'aria-pressed');
    }
  }

  if (element.classList.contains('burger')) {
    burger.classList.toggle("is-active");
    toggleAttribute(burger, 'aria-pressed');
    menu.classList.toggle("is-active");

    //expand aria
    toggleAttribute(menu, 'aria-expanded');

    const menuItemList = document.querySelectorAll('.navbar-item');
    const firstMenuItem = menuItemList[1].querySelector('a');
    //todo implement on key press up
    firstMenuItem.setAttribute('tabindex', -1);
    firstMenuItem.focus();
    firstMenuItem.removeAttribute('tabindex');
  }
}

function toggleAttribute(element, attribute) {
  const currentAttribute = element.getAttribute(attribute);

  if (currentAttribute === true || currentAttribute === 'true') {
    element.setAttribute(attribute, false);
  } else {
    element.setAttribute(attribute, true);
  }
}